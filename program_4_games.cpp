#include "program_4_games.h"

using namespace std;

void menu(int & menu_response) {
  cout  << "1. Add Games \n"
        << "2. Display games by genre \n"
        << "3. Display all games \n"
        << "4. Display games by rating \n"
        << "5. Quit Program \n"
        << "Please select one of the following options (1-5): ";
  cin >> menu_response;
  cin.ignore(100, '\n');
}

// Game constructor
Game::Game() {
  title[0] = '\0';
  genre[0] = '\0';
  num_players = 0;
  review = NULL;
  rating = 0;
  price = 0.0;
}

// Game destructor
Game::~Game() {
  if(review != NULL) {
    delete [] review;
  }
    
}
// Game member function to display game details
void Game::Display_Game() {
  cout << "Title of game: " << title << endl;
  cout << "Genre: " << genre << endl;
  cout << "Number of players: " << num_players << endl;
  cout << "Review: " << review << endl;
  cout << "Rating: " << rating << endl;
  cout << "Price: $" << price << endl;
  cout << "--------------------------------------------------" << endl;
}
// Game member function to read in game details from the user
void Game::Read_Game() {
  cout << "Please enter the game title: ";
  cin.get(title, 100, '\n');
  cin.ignore(100, '\n');
  cout << "Please enter the game genre: ";
  cin.get(genre, 100, '\n');
  cin.ignore(100, '\n');
  cout << "Please enter the number of players: ";
  cin >> num_players;
  cin.ignore(100, '\n');
  char temp[500];
  cout << "Please enter a review: ";
  cin.get(temp, 500, '\n');
  cin.ignore(100, '\n'); 
  review = new char[strlen(temp) + 1];
  strcpy(review, temp);
  cout << "Please enter a rating (1-10): ";
  cin >> rating;
  cin.ignore(100, '\n');
  if(rating < 1 || rating > 10) {
    cout << "Invalid rating. Please enter a rating between 1 and 10: ";
    cin >> rating;
    cin.ignore(100, '\n');
  }
  cout << "Please enter a price ($60.00): $";
  cin >> price;
  cin.ignore(100, '\n');
}


// Game_List constructor
Game_List::Game_List() {
  cout << "Please enter the number of games you want to store: ";
  cin >> size_of_gamesArray;
  cin.ignore(100, '\n');
  gamesArray = new Game[size_of_gamesArray];
  numGames = 0;
  
}

// Game_List destructor
Game_List::~Game_List() {
    
}

// add_games function to read in a new game from the user
void Game_List::add_games() {
char response = 'n';
    do {
        cout << "Would you like to add a game? (y/n) ";
        cin >> response;
        cin.ignore(100, '\n');

        if (tolower(response) == 'y' && numGames < size_of_gamesArray) {
            gamesArray[numGames].Read_Game();
            ++numGames;
        } else if (tolower(response) == 'y') {
            cout << "No more space for additional games." << endl;
        }
    } while (tolower(response) == 'y' && numGames < size_of_gamesArray);
}

// display_games to display all games entered so far
void Game_List::display_games() {
  for (int i = 0; i < numGames; ++i) {
    gamesArray[i].Display_Game();
  }
    
}

// display_by_genre function to display only games that match a particular genre
void Game_List::display_by_genre(char genre[]) {
  for (int i = 0; i < numGames; ++i) {
    if (strcmp(gamesArray[i].genre, genre) == 0) {
      gamesArray[i].Display_Game();
    }
  }
}

// display_by_rating function to display only games that match a particular rating
void Game_List::display_by_rating(int rating) {
  for (int i = 0; i < numGames; ++i) {
    if (gamesArray[i].rating == rating) {
      gamesArray[i].Display_Game();
    }
  }
}

