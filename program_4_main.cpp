#include <iostream>
#include "program_4_games.h"

using namespace std;

int main()
{
    // Main function implementation
    int menu_response; // User's menu choice
    Game_List user_library; // Create a new game list object

    while (menu_response != 5)
    {
        menu(menu_response); // Display the menu and get user's choice
        
        if (menu_response < 1 || menu_response > 5)
        {
            cout << "Please Enter a valid choice";
            menu(menu_response);
        }

        if (menu_response == 1)
        {
            // Add a new games to the user's library
            cout << "You chose: " << menu_response << endl;
            user_library.add_games();
        }
        if (menu_response == 2)
        {
            // Display games for a specific genre
            int NAME = 100;
            char name[NAME]; // Variable to store the name of the shoe to be found
            cout << "Enter the genre of the shoe you are looking for: ";
            cin.get(name, NAME, '\n');
            cin.ignore(100, '\n');
            // user_library.display_by_genre(name);
        }

        if (menu_response == 3)
        {
            // Display all games in user's library
            user_library.display_games();
        }

        if (menu_response == 4)
        {
            // Display games by rating
            int rating;
            cout << "Enter the rating of the game you are looking for (1-10): ";
            cin >> rating;
            cin.ignore(100, '\n');
            // user_library.display_by_rating(rating);
            
        }
    }
    return 0;
}
