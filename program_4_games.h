#include <iostream>

using namespace std;

// Define Game structure/class to hold game details
class Game {
public:
    // Constructor
    Game();
    // Destructor
    ~Game();
    void Display_Game();
    void Read_Game();

    // Member variables
    char title [100];
    char genre [100];
    int num_players;
    char * review;
    int rating;
    float price;
};

// Define Game_List class to manage array of games
class Game_List {
public:
    // Constructor
    Game_List();
    // Destructor
    ~Game_List();

    // Member functions
    void add_games();
    void display_games();
    void display_by_genre(char genre[]);
    void display_by_rating(int rating); // Additional function

private:
    // Private member variables
    Game* gamesArray;
    int size_of_gamesArray;
    int numGames;
};

void menu(int & menu_response);
